# README #

### What is this repository for? ###

* This repository holds all the scripts that one needs in order to complete the data processing for the fishstat schema.
* The commodity\_trpse table, specifically, requires additional manipulation and processing before being passed into the "ult" schema.

### Order and excecution of scripts: ###

1. new\_fishstat\_dc.R
	- This script supercedes and replaces 1.1 and 1.2 listed below. This script includes all of the functionality of the below scripts.
	- Find and replace the fishstat\_18\_v\* reference with relevant new schema name. 
	- If there is no matlab datacleaned version of the data table with which to average in select cases, comment out the ensemble segment before running
	1.1. DatacleanerIndex.sql (archive)
		- Find and replace the name of the commodity\_trpse table that came from your Fishstat Upload.sql.
		- This script creates a "index\_maker\_region" and "index\_maker\_world" tables that will hold regional and world indices that describe year-on-year changes in both directions for each commodity and trade flow.
	1.2. fishstat\_dc.R (archive)
		- Find and replace appropriate table names, including country\_matrix\__year_ 
		- In the 'index\_region\_world' query, change the year to the one right after the most recent data.
		- This script applies linear interpolation between real data points, and uses regional (if available) or world indices to front- or back-fill from the closest observation to the end of the timeline.

2. post\_DC\_and\_ensemble\_FFIL\_and\_split.sql
	- This script is designed to 
		- Average with a different data cleaner, which is likely not available any more.
		- Edit datacleaned records for specific country-commodity-trade flows.
		- Naively front-fill values to the most recent NFA data year, if necessary.
		- Aggregate values for China and put the other countries into the hk\_etc schema.
	- If not already done, comment out the ensemble part of the script.
	- Check the raw data for the presence of the issues that are pointed to in the manual data cleaner part. Some may no longer need to be fixed given changes to raw data (one can always comment out the manual data cleaner until the issue still negatively impacts footprint timeline)

### Who do I talk to? ###

* Latest editor/user: David Lin
* Previous editor/user: Evan Neill, Dengyao Xu, Jon Martindill
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)