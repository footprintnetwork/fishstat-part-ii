#################################################
# DATA CLEANER PROJECT: INDEX MAKER
# This code creates an index for commodity trades from Fishstat(FAO)
#	This must be run before running fishstat_dc.
#
# tables needed
#	`country_data`.`country_matrix_2019`
#	`fishstat_19_baseline_v1_sql`.`commodity_trpse`
#
# tables created:
#	`fishstat_19_baseline_v1_sql`.`index_maker_world`
#	`fishstat_19_baseline_v1_sql`.`index_maker_region`
#
# Update: the latest year should be updated in where clause for the insert statement at the end.
#
#################################################


#############
# FISHSTAT
#############

  USE fishstat_19_baseline_v1_sql\p;

# index_maker world
  DROP TABLE IF EXISTS index_maker_world\p;
  CREATE TABLE index_maker_world
  ( year INT,
    trade_flow varchar(10) NOT NULL,
    UN_region varchar(100) NOT NULL,
    HS_code double NOT NULL,
    next_value numeric(20,2) ,
    cur_value numeric(20,2) ,
    growth_index double ,
    FFIL_index double,
    CONSTRAINT PK_pr PRIMARY KEY (year,trade_flow,HS_code,UN_region)
  )\p;

# index_maker_region
  DROP TABLE IF EXISTS index_maker_region \p;
  CREATE TABLE index_maker_region
  ( year INT,
    trade_flow varchar(10) NOT NULL,
    UN_region varchar(100) NOT NULL,
    HS_code double NOT NULL,
    next_value numeric(20,2),
    cur_value numeric(20,2),
    growth_index double,
    FFIL_index double,
    CONSTRAINT PK_pr PRIMARY KEY (year,trade_flow,HS_code,UN_region)
  )\p;

### Regional
  TRUNCATE TABLE fishstat_19_baseline_v1_sql.index_maker_region \p;

# Create the table across all regions, years, commodity codes, and trade flows. Will have to get rid of some observations later.
  INSERT INTO fishstat_19_baseline_v1_sql.index_maker_region
  SELECT
      year, trade_flow,UN_region, HS_code,  NULL, 0 as `cur_value`, NULL, NULL
  FROM
      (SELECT DISTINCT
          year
      FROM
          fishstat_19_baseline_v1_sql.commodity_trpse) a
          CROSS JOIN
      (SELECT DISTINCT
          trade_flow
      FROM
          fishstat_19_baseline_v1_sql.commodity_trpse) d
          CROSS JOIN
      (SELECT DISTINCT
          UN_region
      FROM
          country_data.country_matrix_2019
          where UN_region!='World') c
          CROSS JOIN
      (SELECT DISTINCT
          HS_code
      FROM
          fishstat_19_baseline_v1_sql.commodity_trpse) b
  ORDER BY trade_flow , UN_region , HS_code,year DESC \p;

  # Insert the regional tonnage values in each year
  UPDATE fishstat_19_baseline_v1_sql.index_maker_region a JOIN
  (SELECT
      a.year,
      a.trade_flow,
      UN_region,
      a.HS_code,
      SUM(a.value) AS `cur_value`
  FROM
      fishstat_19_baseline_v1_sql.commodity_trpse a
      LEFT JOIN
      country_data.country_matrix_2019 c using(country_code)
      GROUP BY UN_region , year , trade_flow , HS_code) b using(year,trade_flow,UN_region,HS_code)
  SET a.`cur_value`=b.`cur_value` where b.`cur_value` IS NOT NULL \p;

# Insert the next years tonnage in each observation.
  UPDATE fishstat_19_baseline_v1_sql.index_maker_region a JOIN
  fishstat_19_baseline_v1_sql.index_maker_region b on a.UN_region=b.UN_region and a.trade_flow=b.trade_flow and a.HS_code=b.HS_code and a.year=b.year-1
  set a.`next_value`=b.`cur_value` \p;

# Calculate the 'growth index' and 'FFIL_index'
  UPDATE fishstat_19_baseline_v1_sql.index_maker_region
  SET
      growth_index = cur_value / next_value,
      FFIL_index = next_value / cur_value
  ORDER BY UN_region , HS_code , trade_flow , year DESC \p;

# Make temp table to insert max and min year values for each HS_code, region, and trade_flow\
  DROP TABLE IF EXISTS region_index_minmax_yr \p;
  create table region_index_minmax_yr (
  HS_code double,
  UN_region varchar(100),
  trade_flow varchar(20),
  minyear int,
  maxyear int,
  PRIMARY KEY (trade_flow,HS_code,UN_region)
  )\p;

  INSERT INTO region_index_minmax_yr select
      HS_code, UN_region, trade_flow, MIN(year) as minyear, MAX(year) as maxyear
  FROM
      fishstat_19_baseline_v1_sql.index_maker_region
  WHERE
     cur_value>0
  GROUP BY HS_code , UN_region , trade_flow \p;

# Get rid of values that should not be in the table. Since cur_value is default 0, this should be specific to only trouble years.
  UPDATE fishstat_19_baseline_v1_sql.index_maker_region a left join
  region_index_minmax_yr b using(HS_code,UN_region,trade_flow)
  set a.`cur_value`=NULL where (a.year<b.minyear or a.year>b.maxyear or b.minyear IS NULL)\p;

  DELETE FROM fishstat_19_baseline_v1_sql.index_maker_region where cur_value IS NULL \p;

  DROP TABLE region_index_minmax_yr \p;

  /* SELECT ft.*,ft.cur_value/ft.next_value AS growth_index,ft.next_value/ft.cur_value AS FFIL_index
  FROM
  	(SELECT
      a.year,
      a.trade_flow,
      UN_region,
      a.HS_code,
  	SUM(d.value) AS `next_value`,
      SUM(a.value) AS `cur_value`
  FROM
      fishstat_19_baseline_v1_sql.commodity_trpse a
          LEFT JOIN
      fishstat_19_baseline_v1_sql.commodity_trpse d using(country_code,trade_flow,HS_code)
          LEFT JOIN
      country_data.country_matrix_2019 c using(country_code)
      where a.year=d.year-1
  GROUP BY UN_region , year , trade_flow , HS_code) ft
  ORDER BY UN_region,HS_code,trade_flow,year DESC \p;
  */
