##################################################################################################################################
# This script comes after R data cleaner. Right now here's what it does:
# 	1) Create an ensemble of cleaned fish trade data with commodity_ult from last year. This is because matlab DC wasn't run this year.
#		This may not be necessary in following years with new editions of fish trade data
# 	2) Apply "manual data cleaner" fixes that were necessary for NFA 2017 edition, and also fishstat baselines in the run up to NFA 2018 because the trade data hadn't changed.
# 		again, this may not be necessary in following years with new editions of fish trade data
# 	3) Front fill the fish trade data naively to 2015 (use 2013 values as 2014 and 2015) in order to provide a new year of data.
# 		This may be altered in the near future to scale with country production  & world exports if no data is released by end of fishstat baseline production
# 	4) Apply the China aggsplit protocol.
#
# 		-Evan Neill 9/29/17
#
#	INPUT:
#		fishstat_18_v4_ult.commodity_r_dt_ult_test  (R data cleaned)
#		fishstat_17_v1_ult.commodity_ult			(matlab data cleaned) # Not as of 10/24/17 - DNE yet for new trade data
#		
#	OUTPUT:
#		fishstat_18_v4_ult.commodity_r_dt_ult		(average for select countries but without any manual fixes. Will not be used)
#		fishstat_18_v4_ult.commodity_mdcf			(This will get naive frontfill then china aggsplit)
#		fishstat_18_v4_ult_hketc.commodity_mdcf		
#
##################################################################

## Fishstat Commodity Ensemble Part ##
/*USE `fishstat_18_v4_ult`;
DROP TABLE IF EXISTS `commodity_r_dt_ult`\p;
CREATE TABLE `commodity_r_dt_ult` like `fishstat_18_v4_ult`.`commodity_r_dt_ult_test`\p;
INSERT INTO `commodity_r_dt_ult` 
SELECT  
                a.country,  
                a.country_code,   
                a.commodity,   
                a.HS_Code,  
				a.trade_flow,
                a.units,
                a.year,
                IF((country_code IN (158,86,136,32,104,109,20,45,58) AND a.dcflag !='R'),(ifnull(a.value,0) + ifnull(b.value,0))/2, a.value) as `value`,  
                a.fao_flag,     
                a.dcflag
FROM `fishstat_18_v4_ult`.`commodity_r_dt_ult_test` a  
LEFT JOIN `fishstat_17_v1_ult`.`commodity_ult` b   
USING(country_code, year, hs_code, trade_flow)\p;


*/
##########################################################################
#2. Manual data cleaning part #
USE fishstat_18_v4_ult;

DROP TABLE IF EXISTS commodity_mdcf\p;  ##UPDATE
CREATE TABLE commodity_mdcf LIKE fishstat_18_v4_ult.commodity_r_dt_ult\p;
INSERT INTO commodity_mdcf SELECT * FROM fishstat_18_v4_ult.commodity_r_dt_ult\p;
UPDATE commodity_mdcf
SET commodity_mdcf.value=if(( 
(country_code=185 and HS_code in (303.55,303.67,303.63) and trade_flow='Export' and GFN_flag in ('BFIL')) 
or (country_code=9 and HS_code in (304.74) and trade_flow='Export' and GFN_flag!='R') 
or (country_code=35 and HS_code in (306.29,304.99) and trade_flow='Import' and GFN_flag!='R')
)
,0,commodity_mdcf.value)\p;
UPDATE commodity_mdcf 
SET commodity_mdcf.GFN_flag=if((
(country_code=185 and HS_code in (303.55,303.67,303.63) and trade_flow='Export' and GFN_flag in ('BFIL'))  
or (country_code=9 and HS_code in (304.74) and trade_flow='Export' and GFN_flag!='R')  
or (country_code=35 and HS_code in (306.29,304.99) and trade_flow='Import' and GFN_flag!='R')
) 
,'MDCF',commodity_mdcf.GFN_flag)\p;

###########################################################################
#	Create commodity key table in the ult schema

DROP TABLE IF EXISTS commodity_key\p;
CREATE TABLE commodity_key (
HS_code double,
commodity varchar(120),
PRIMARY KEY (HS_code)
) engine=MyISAM DEFAULT CHARSET=latin1\p;

INSERT INTO commodity_key
select distinct HS_code,commodity from commodity_mdcf where commodity!='' order by HS_code\p;# anticipates blanks from DC.

##########################################################################
## Naive front fill part to get new data year
/*
alter table fishstat_18_v4_ult.commodity_mdcf add column GFN_flag varchar(5)\p;

INSERT IGNORE INTO fishstat_18_v4_ult.commodity_mdcf (SELECT `commodity_mdcf`.`country`,
    `commodity_mdcf`.`country_code`,
    `commodity_mdcf`.`commodity`,
    `commodity_mdcf`.`HS_code`,
    `commodity_mdcf`.`trade_flow`,
    `commodity_mdcf`.`units`,
    2014 as `year`,
    `commodity_mdcf`.`value`,
    `commodity_mdcf`.`fao_flag`,
    `commodity_mdcf`.`dcflag`,
    'NCFIL' as `GFN_flag` FROM fishstat_18_v4_ult.commodity_mdcf where year = 2013)\p;

INSERT IGNORE INTO fishstat_18_v4_ult.commodity_mdcf (SELECT `commodity_mdcf`.`country`,
    `commodity_mdcf`.`country_code`,
    `commodity_mdcf`.`commodity`,
    `commodity_mdcf`.`HS_code`,
    `commodity_mdcf`.`trade_flow`,
    `commodity_mdcf`.`units`,
    2015 as `year`,
    `commodity_mdcf`.`value`,
    `commodity_mdcf`.`fao_flag`,
    `commodity_mdcf`.`dcflag`,
    'NCFIL' as `GFN_flag` FROM fishstat_18_v4_ult.commodity_mdcf where year = 2014)\p;

*/
    
##########################################################################
## China aggsplit part

#2. Fishstat Commodity aggregation
INSERT INTO fishstat_18_v4_ult.commodity_mdcf
SELECT 'China',351,commodity,HS_code,trade_flow,units,year,sum(value),fao_flag,'CnAG' 
FROM fishstat_18_v4_ult.commodity_mdcf
WHERE country_code IN (41,96,214,128)
GROUP BY HS_code, trade_flow, year\p;

USE fishstat_18_v4_ult_hketc;
DROP TABLE IF EXISTS commodity_mdcf;
CREATE TABLE commodity_mdcf like fishstat_18_v4_ult.commodity_mdcf;
INSERT INTO commodity_mdcf SELECT * FROM fishstat_18_v4_ult.commodity_mdcf
WHERE country_code in (41, 96, 128, 214)\p;

DELETE FROM fishstat_18_v4_ult.commodity_mdcf
WHERE country_code in (41, 96, 128, 214)\p;